package com.itheima.bean;

import com.itheima.utils.MyBeanUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class TestCustomer {
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        //获取前台录入的数据  map
        Map<String, String[]> map = new HashMap<>();
        map.put("name", new String[]{"xiazdong"});
        map.put("sex",  new String[]{"man"});
        map.put("birthday", new String[]{"2010-10-10"});
        //创建bean
        Customer cus = new Customer();
        //date转换
        DateConverter dateConverter = new DateConverter();
        dateConverter.setPattern("yyyy-MM-dd");
        ConvertUtils.register(dateConverter, java.util.Date.class);
        //把map中的数据拷贝到bean中
        BeanUtils.populate(cus, map);
        System.out.println(cus.getName());
        //测试通过
        System.out.println(cus.getBirthday());//Sun Oct 10 00:00:00 CST 2010

        //使用工具类
        testMyBeanUtils();
    }

    private static void testMyBeanUtils() {
        //获取前台录入的数据  map
        Map<String, String[]> map = new HashMap<>();
        map.put("name", new String[]{"xiazdong"});
        map.put("sex",  new String[]{"man"});
        map.put("birthday", new String[]{"2019-10-28"});
        Customer populate = MyBeanUtils.populate(Customer.class, map);
        System.out.println(populate.getName());
        System.out.println(populate.getBirthday());//Mon Oct 28 00:00:00 CST 2019
    }
}
