package com.itheima.service;

import java.sql.SQLException;

import com.itheima.bean.Customer;
import com.itheima.dao.RegisterDao;

public class RegisterService {

	public void saveCustomer(Customer cus) throws SQLException {
		//创建RegisterDao
		RegisterDao rd = new RegisterDao();
		//调用方法
		rd.saveCustomer(cus);
	}

}
