package com.itheima.utils;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Map;

public class MyBeanUtils {
    public static <T> T populate (Class<T> beanClass , Map<String,String[]> properties) {
        try {
            //1反射创建实例
            T bean = beanClass.newInstance();
            //2时间转换器
            DateConverter dateConverter = new DateConverter();
            dateConverter.setPattern("yyyy-MM-dd");
            //3注册转换器
            ConvertUtils.register(dateConverter, Date.class);
            BeanUtils.populate(bean,properties);
            return bean;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
