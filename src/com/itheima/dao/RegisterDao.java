package com.itheima.dao;

import java.sql.SQLException;

import org.apache.commons.dbutils.QueryRunner;

import com.itheima.bean.Customer;
import com.itheima.utils.DataSourceUtils;

public class RegisterDao {

	public void saveCustomer(Customer cus) throws SQLException {
		//创建QueryRunner对象
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		//编写sql
		String sql = "insert into customer values(null,?,?,?,?,?,?)";
		//执行sql
		qr.update(sql, cus.getUsername(),cus.getPassword(),cus.getEmail(),cus.getName(),cus.getSex(),cus.getBirthday());
	}

}
