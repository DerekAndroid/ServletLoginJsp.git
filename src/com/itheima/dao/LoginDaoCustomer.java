package com.itheima.dao;

import com.itheima.bean.Customer;
import com.itheima.bean.User;
import com.itheima.utils.DataSourceUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.sql.SQLException;

public class LoginDaoCustomer {

	public Customer getUserByUsernameAndPwd(String username, String password) throws SQLException {
		//创建QueryRunner
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		//编写sql
		String sql ="select * from customer where username=? and password = ?";
		//执行sql
		Customer user = qr.query(sql, new BeanHandler<Customer>(Customer.class), username,password);

		return user;
	}

}
