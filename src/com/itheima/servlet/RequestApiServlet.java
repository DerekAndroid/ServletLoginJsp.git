package com.itheima.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RequestApiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//获取请求的方式
		String method = request.getMethod();
		System.out.println(method);
		//获取项目的动态路径
		String path = request.getContextPath();
		System.out.println(path);
		//获取请求者的ip
		String addr = request.getRemoteAddr();
		System.out.println(addr);
		//获取请求的资源(不带参数)
		String uri = request.getRequestURI();
		System.out.println(uri);
		//获取请求的参数
		String queryString = request.getQueryString();
		System.out.println(queryString);
		//获取协议和版本
		String protocol = request.getProtocol();
		System.out.println(protocol);
		
		System.out.println(method+"  "+uri+"?"+queryString+"  "+protocol);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
