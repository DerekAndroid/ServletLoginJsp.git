package com.itheima.lmServlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//这种方式get提交中文会出现乱码
		//从post方法中移过来的
//		request.setCharacterEncoding("utf-8");
//		//获取username
//		String username = request.getParameter("username");
//		System.out.println(username);
//		//获取hobby
//		String[] hobbys = request.getParameterValues("hobby");
//		System.out.println(Arrays.toString(hobbys));
//		//获取所有
//		Map<String, String[]> map = request.getParameterMap();
//		for(String key:map.keySet()){
//			System.out.println(key+":"+Arrays.toString(map.get(key)));
//		}
		
		//获取username
		String username = request.getParameter("username");
		System.out.println(new String(username.getBytes("iso-8859-1"),"utf-8"));
		//获取hobby
//		String[] hobbys = request.getParameterValues("hobby");
//		System.out.println(Arrays.toString(hobbys));
		//获取所有
		Map<String, String[]> map = request.getParameterMap();
		for(String key:map.keySet()){
			System.out.println(key+":"+new String(Arrays.toString(map.get(key)).getBytes("iso-8859-1"),"utf-8"));
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		//		//针对于post请求  乱码解决方式
//		request.setCharacterEncoding("utf-8");
//		//获取username
//		String username = request.getParameter("username");
//		System.out.println(username);
//		//获取hobby
//		String[] hobbys = request.getParameterValues("hobby");
//		System.out.println(Arrays.toString(hobbys));
//		//获取所有
//		Map<String, String[]> map = request.getParameterMap();
//		for(String key:map.keySet()){
//			System.out.println(key+":"+Arrays.toString(map.get(key)));
//		}
	}

}
