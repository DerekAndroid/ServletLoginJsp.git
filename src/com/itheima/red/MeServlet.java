package com.itheima.red;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String money = request.getParameter("money");
		
		System.out.println("我说：咋了");
		System.out.println("甲说：没钱，借"+money);
		System.out.println("我说：没有");
		System.out.println("我说：我去帮你借！");
		//把money放入request域中
		request.setAttribute("moneyx", money);
		//请求转发到bzr
		request.getRequestDispatcher("/bzr").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
